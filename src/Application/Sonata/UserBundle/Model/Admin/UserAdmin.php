<?php


namespace App\Application\Sonata\UserBundle\Model\Admin;

use App\Services\FileUploader;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends SonataUserAdmin
{
    private $container;
    private $fileUploader;

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
    }


    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        parent::configureFormFields($formMapper);

        $this->container = $this->getConfigurationPool()->getContainer();
        $this->fileUploader = $this->container->get('App\Services\FileUploader');
        $user = $this->getSubject();

        $pictureFormFieldsOptions = [
            'label' => 'Image',
            'mapped' => false,
            'required' => false,
            'data_class' => null
        ];

        if($picture = $user->getPicture()){
            if($picture->getName()){
                $uploadDirectory = $this->fileUploader->getUploadDirWebPath() . '/users';
                $webPath = $uploadDirectory . '/' . $picture->getName();
            }else{
                $imagesDirectory = $this->container->getParameter('images_dir_webpath');
                $defaultImage = $this->container->getParameter('user_default_picture');
                $webPath = $imagesDirectory . '/' . $defaultImage;
            }

            $pictureFormFieldsOptions['help'] = '<img src="'.$webPath.'" class="admin-preview"/>';
        }

        $formMapper
            ->tab('User')
            ->with('Profile')
            ->add('dateOfBirth', BirthdayType::class, [
                'required' => false,
            ])
            ->add('biography', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '8'
                ]
            ])
            ->add('job', TextType::class, [
                'label' => 'Fonction',
                'required' => false,
            ])
            ->add('pictureTmp', FileType::class, $pictureFormFieldsOptions)
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('username', TextType::class, [
                'label' => 'Utilisateur'
            ])
            ->add('fullname', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('picture.name', TextType::class, [
                'label' => 'Image'
            ])
            ->add('_action', null, [
                'label' => 'Actions',
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => []
                ]
            ])
        ;
    }
}