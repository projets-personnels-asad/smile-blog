<?php


namespace App\Services;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class FileUploader
{
    private $uploadDirFullPath;
    private $uploadDirWebPath;

    public function __construct($uploadDirFullPath, $uploadDirWebPath)
    {
        $this->uploadDirFullPath = $uploadDirFullPath;
        $this->uploadDirWebPath = $uploadDirWebPath;
    }

    public function uploadFile(File $file, $directory = "")
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        try {
            $file->move($this->uploadDirFullPath . '/' . $directory, $fileName);
            return $fileName;
        } catch (FileException $e) {

        }

        return null;
    }

    public function removeFile($fileName, $directory = ""){
        $filesystem = new Filesystem();
        $fullPath = $this->uploadDirFullPath;

        if($directory !== ""){
            $fullPath .= '/' . $directory;
        }

        $fullPath .= '/' . $fileName;

        if($filesystem->exists($fullPath)){
            $filesystem->remove([$fullPath]);
            return true;
        }
        return false;
    }

    public function getUploadDirFullPath()
    {
        return $this->uploadDirFullPath;
    }

    public function getUploadDirWebPath()
    {
        return $this->uploadDirWebPath;
    }
}