<?php


namespace App\Events\EntityListener;


use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Services\FileUploader;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ArticleListener implements EventSubscriber
{
    private $fileUploader;
    private $container;
    private $entityManager;

    public function __construct(FileUploader $fileUploader, ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->fileUploader = $fileUploader;
        $this->container = $container;
        $this->entityManager = $entityManager;
    }


    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove
        ];
    }

    public function prePersist(Article $article, LifecycleEventArgs $args)
    {
        $article->setPublishDate(new \DateTime());
        $article->setUpdateDate(new \DateTime());

        if($article->getImage()){
            $fileName = $this->uploadFile($article->getImage());
            $article->setImage($fileName);
            $article->setImageName($fileName);
        }
    }

    public function preUpdate(Article $article, LifecycleEventArgs $args)
    {
        $article->setUpdateDate(new \DateTime());

        if($article->getImage()){
            if($article->getImageName()){
                $this->removeFile($article->getImageName());
            }
            $newFileName = $this->uploadFile($article->getImage());
            $article->setImage($newFileName);
            $article->setImageName($newFileName);
        }else{
            if($article->getImageName()){
                $article->setImage($article->getImageName());
            }
        }

/*        if($article->getIsMainArticle()){
            $mainArticle = $this->entityManager->getRepository(Article::class)->getMainArticle();
            if($mainArticle){
                $mainArticle->setIsMainArticle(false);
                $this->entityManager->persist($mainArticle);
                $this->entityManager->flush();
            }
        }*/
    }


    public function preRemove(Article $article, LifecycleEventArgs $args)
    {
        if($article->getImage()){
            $this->removeFile($article->getImageName());
        }
    }

    private function uploadFile(string $tmpFile):string
    {
        $image = new File($tmpFile);
        $uploadDir = $this->fileUploader->getTargetDirectory() . '/articles/';
        $fileName = $this->fileUploader->uploadFile($image, $uploadDir);
        return $fileName;
    }

    private function removeFile(?string $filename)
    {
        $filePath = $this->fileUploader->getTargetDirectory() . '/articles/' . $filename;
        $this->fileUploader->removeFile($filePath);
    }
}
