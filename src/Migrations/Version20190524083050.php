<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190524083050 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fos_user_picture (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, full_path VARCHAR(255) DEFAULT NULL, web_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fos_user_user ADD picture_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D761EE45BDBF FOREIGN KEY (picture_id) REFERENCES fos_user_picture (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C560D761EE45BDBF ON fos_user_user (picture_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D761EE45BDBF');
        $this->addSql('DROP TABLE fos_user_picture');
        $this->addSql('DROP INDEX UNIQ_C560D761EE45BDBF ON fos_user_user');
        $this->addSql('ALTER TABLE fos_user_user DROP picture_id');
    }
}
