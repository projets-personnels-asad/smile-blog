// webpack.config.js
var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('public/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // will create public/build/app.js and public/build/app.css
    .addEntry('reset','./public/themes/letsblog/css/reset.css')
    .addEntry('wordpress', './public/themes/letsblog/css/wordpress.css')
    .addEntry('animation', './public/themes/letsblog/css/animation.css')
    //.addEntry('magnific-popup' ,'./public/themes/letsblog/css/magnific-popup.css')
    .addEntry('jquery-custom','./public/themes/letsblog/css/jqueryui/custom.css')
    //.addEntry('flexslider', './public/themes/letsblog/js/flexslider.css')
    //.addEntry('tooltipster', './public/themes/letsblog/css/tooltipster.css')
    .addEntry('screen', './public/themes/letsblog/css/screen.css')
    //.addEntry('custom', './public/themes/letsblog/css/custom.css')
    .addEntry('form-basic', './public/themes/letsblog/css/form-basic.min.css')
    .addEntry('kirki-styles', './public/themes/letsblog/css/kirki-styles.css')
    .addEntry('homepage', './public/assets/css/home.css')
    .addEntry('list-articles', './public/assets/css/list_articles.css')
    .addEntry('profile', './public/assets/css/profile.css')
    .addEntry('style', './public/assets/css/style.css')

    .addEntry('wp-emoji-release', './public/themes/letsblog/js/wp-emoji-release.min.js')
    .addEntry('jquery', './public/themes/letsblog/js/jquery/jquery.js')
    .addEntry('jquery-migrate', './public/themes/letsblog/js/jquery/jquery-migrate.min.js')

    .addEntry('frame', './public/themes/letsblog/css/frame.css')

    .addEntry('magnific-popup', './public/themes/letsblog/js/magnific-popup.js')
    .addEntry('easing', './public/themes/letsblog/js/easing.js')
    .addEntry('waypoints', './public/themes/letsblog/js/waypoints.min.js')
    .addEntry('isotope', './public/themes/letsblog/js/isotope.js')
    .addEntry('tooltipster', './public/themes/letsblog/js/tooltipster.min.js')
    .addEntry('custom-plugins', './public/themes/letsblog/js/custom_plugins.js')
    .addEntry('custom', './public/themes/letsblog/js/custom.js')
    .addEntry('wp-embed', './public/themes/letsblog/js/wp-embed.min.js')

    .addEntry('flexslider', './public/themes/letsblog/js/flexslider.js')
    .addEntry('slider-flexslider', './public/themes/letsblog/js/slider-flexslider.js')
    .addEntry('cookie', './public/themes/letsblog/js/cookie.js')
    .addEntry('demo', './public/themes/letsblog/js/demo.js')
    .addEntry('forms-api', './public/themes/letsblog/js/forms-api.min.js')
    .addEntry('placeholders', './public/themes/letsblog/js/placeholders.min.js')
    .addEntry('bootstrap', './public/assets/bootstrap-4.3.1-dist/js/bootstrap.min.js')

    // allow sass/scss files to be processed
    .enableSassLoader(function(sassOptions) {}, {
        resolveUrlLoader: false
    })

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

// show OS notifications when builds finish/fail
// create hashed filenames (e.g. app.abc123.css)
// .enableVersioning()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
