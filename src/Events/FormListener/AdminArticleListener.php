<?php


namespace App\Events\FormListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AdminArticleListener implements EventSubscriberInterface
{

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT => 'onPrePostData',
            FormEvents::POST_SET_DATA => 'onPostSetData'
        ];
    }

    public function onPreSetData(FormEvent $event)
    {
        $article = $event->getData();
        $form = $event->getForm();
    }

    public function onPrePostData(FormEvent $event){
        $article = $event->getData();
        $form = $event->getForm();

    }

    public function onPostSetData(FormEvent $event)
    {

    }
}