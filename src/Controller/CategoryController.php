<?php

namespace App\Controller;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function sidebarList()
    {
        $repository = $this->em->getRepository(Category::class);
        $categories = $repository->findAll();

        return $this->render('category/sidebar_categories.html.twig', array(
            'categories' => $categories,
        ));
    }

    public function menuList()
    {
        $repository = $this->em->getRepository(Category::class);
        $categories = $repository->findAll();

        return $this->render('category/menu_categories.html.twig', array(
            'categories' => $categories,
        ));
    }
}
