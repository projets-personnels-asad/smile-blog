<?php


namespace App\Admin;


use App\Entity\Article;
use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Filter\DateFilter;
use Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AdminComment extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user.fullname', TextType::class, [
                'label' => 'Utilisateur'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
                'attr' => [
                    'rows' => '10'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Valide',
                'choices'  => [
                    'Non' => false,
                    'Oui' => true,
                ],
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user')
            ->add('status')
            ->add('publishDate', DateFilter::class)
            ->add('updateDate', DateFilter::class);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('user.fullname', TextType::class, [
                'label' => 'Utilisateur'
            ])
            ->add('article.title', TextType::class, [
                'label' => 'Article'
            ])
            ->add('status', null, [
                'label' => 'Status'
            ])
            ->add('publishDate', null, [
                'label' => 'Date publication'
            ])
            ->add('updateDate', null, [
                'label' => 'Date édition'
            ])
        ;
    }
}