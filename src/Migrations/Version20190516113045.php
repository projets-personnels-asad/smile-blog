<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190516113045 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact');
        $this->addSql('ALTER TABLE article CHANGE publish_date publish_date DATETIME NOT NULL, CHANGE update_date update_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('DROP INDEX IDX_9474526CA76ED395 ON comment');
        $this->addSql('ALTER TABLE comment DROP user_id, CHANGE content content VARCHAR(255) NOT NULL, CHANGE publish_date publish_date DATETIME NOT NULL, CHANGE update_date update_date DATETIME NOT NULL, CHANGE status status INT NOT NULL');
        $this->addSql('ALTER TABLE fos_user_user DROP picture, DROP picture_name, DROP job');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, message LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE article CHANGE publish_date publish_date DATE NOT NULL, CHANGE update_date update_date DATE NOT NULL');
        $this->addSql('ALTER TABLE comment ADD user_id INT NOT NULL, CHANGE content content TINYTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE publish_date publish_date DATE NOT NULL, CHANGE update_date update_date DATE NOT NULL, CHANGE status status TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_9474526CA76ED395 ON comment (user_id)');
        $this->addSql('ALTER TABLE fos_user_user ADD picture VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD picture_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD job VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
